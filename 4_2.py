map = [
    [0, 0, 1, 0],
    [0, 0, 0, 0],
    [0, 0, 1, 0],
    [0, 1, 0, 0],
    [0, 0, 0, 1]
]
aim = [3, 2]
start = [0, 0]
track = []
global_min_step = [100000]


def dfs(x: int, y: int, step: int):
    track.append([x, y])
    if [x, y] == aim:
        print("step:", step)
        print("success track is: ", track)
        if step < global_min_step[0]:
            global_min_step[0] = step
        return
    else:
        nexts = [[x, y + 1], [x + 1, y], [x, y - 1], [x - 1, y]]

        for next_item in nexts:
            next_item_x = next_item[0]
            next_item_y = next_item[1]
            # print(x, y)
            if not (next_item_x < 0 or next_item_x > (len(map) - 1) or next_item_y < 0 or next_item_y > (
                    len(map[0]) - 1)):
                next_map_point = map[next_item_x][next_item_y]
                can_go_next_dfs = (next_map_point != 1) and ([next_item[0], next_item[1]] not in track)

                if can_go_next_dfs:
                    dfs(next_item[0], next_item[1], step + 1)


if __name__ == '__main__':
    dfs(0, 0, 0)
    print("global_min_step: ", global_min_step)
