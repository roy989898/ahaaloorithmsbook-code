def have_duplicate(nums: [int]):
    num_1 = len(nums)
    myset = set(nums)
    num_2 = len(myset)
    return num_2 != num_1


def my_loop(n: int, count: int):
    result = []
    if count <= 0:
        return []
    right_parts = my_loop(n, count - 1)
    if len(right_parts) == 0:
        for num in range(1, n + 1, 1):
            r = [num]
            result.append(r)
    else:
        for num in range(1, n + 1, 1):
            for right_part in right_parts:
                r = [num] + right_part
                result.append(r)

    return result


if __name__ == '__main__':
    num_str_arr = []
    n = 4
    answer_in_arr_arr = my_loop(n, n)
    for answer_in_arr in answer_in_arr_arr:
        if not have_duplicate(answer_in_arr):
            r = ''.join(map(str, answer_in_arr))
            num_str_arr.append(r)

    print(num_str_arr)
