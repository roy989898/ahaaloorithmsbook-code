from Queue import Queue


def qq_num(nums: [int]):
    result_arr = []
    while len(nums) > 0:
        to_result_arr_num = nums.pop(0)
        result_arr.append(to_result_arr_num)
        if len(nums) > 0:
            to_arr_tail_num = nums.pop(0)
            nums.append(to_arr_tail_num)
    return result_arr


def qq_num_improved_in_book(nums: [int]):
    answer = []
    new_array = [0] * 101
    # init the array
    q = Queue()
    q.head = 0
    q.tail = len(nums)
    # q.new_array = new_array
    for index, num in enumerate(nums):
        q.data[index] = num

    print(q.data)

    while (q.tail - q.head) > 0:
        # print("new_array: ", new_array)
        # print("head: ", head)
        # print("tail: ", tail)
        # print("new_array: ", new_array[head:tail + 1])
        removed_num = q.data[q.head]
        q.head = q.head + 1
        answer.append(removed_num)
        to_tail_num = q.data[q.head]
        q.head = q.head + 1
        q.tail = q.tail + 1
        print(q.tail - 1)
        q.data[q.tail - 1] = to_tail_num

    return answer


if __name__ == '__main__':
    password = [6, 3, 1, 7, 5, 8, 9, 2, 4]
    # qq_num_improved_in_book(password)
    print(qq_num_improved_in_book(password))
