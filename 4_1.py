book = [True] * 10
a = [0] * 10


def dfs(step: int, n: int):
    if step > n:
        print(a)
        return
    for i_n in range(n + 1):
        if book[i_n]:
            a[step] = i_n
            book[i_n] = False
            dfs(step + 1, n)
            book[i_n] = True
    return


if __name__ == '__main__':
    dfs(0, 3)
