import math

map = [
    [0, 2, 6, 4],
    [math.inf, 0, 3, math.inf],
    [7, math.inf, 0, 1],
    [5, math.inf, 12, 0],
]
x_max_index = len(map) - 1
y_max_index = len(map[0]) - 1

if __name__ == '__main__':
    for point_can_pass in range(x_max_index + 1):
        for x in range(x_max_index + 1):
            for y in range(y_max_index + 1):
                added_point_shortest_distance = map[x][point_can_pass] + map[point_can_pass][y]
                if map[x][y] > added_point_shortest_distance:
                    map[x][y] = added_point_shortest_distance

    print(map)
