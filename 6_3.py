import math

# A->B, D
point_and_edge = [
    [1, 2, 2],
    [0, 1, -3],
    [0, 4, 5],
    [3, 4, 2],
    [2, 3, 3],

]
dis = [0, math.inf, math.inf, math.inf, math.inf]
point_no = 5
if __name__ == '__main__':
    for i in range(point_no - 1):
        for edge_index in range(len(point_and_edge)):
            distance_point = point_and_edge[edge_index][1]
            start_point = point_and_edge[edge_index][0]
            edge_distance = point_and_edge[edge_index][2]
            distance_point_old_dis = dis[distance_point]
            distance_point_new_dis = dis[start_point] + edge_distance
            if distance_point_new_dis < distance_point_old_dis:
                dis[distance_point] = distance_point_new_dis

    print(dis)
