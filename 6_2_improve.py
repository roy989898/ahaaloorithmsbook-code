import math

map = [
    [0, 1, 12, math.inf, math.inf, math.inf],
    [math.inf, 0, 9, 3, math.inf, math.inf],
    [math.inf, math.inf, 0, math.inf, 5, math.inf],
    [math.inf, math.inf, 4, 0, 13, 15],
    [math.inf, math.inf, math.inf, math.inf, 0, 4],
    [math.inf, math.inf, math.inf, math.inf, math.inf, 0],

]
dis = [0, 1, 12, math.inf, math.inf, math.inf]
start_point = 0
handled_point = [start_point]


def shortest_distance_point(handled_point: [int]):
    point = 0
    shortest_distance = math.inf
    for point_in_dis, distance in enumerate(dis):
        if point_in_dis not in handled_point and distance < shortest_distance:
            point = point_in_dis
            shortest_distance = distance

    return point


def find_connected_points(cur: int):
    point = []
    distancess = map[cur]
    for possible_point, distances in enumerate(distancess):
        if distances != math.inf:
            point.append(possible_point)
    return point


if __name__ == '__main__':
    point_no = len(map[0])
    while len(handled_point) < point_no:
        point_that_shortest_distance_no_in_handled_point = shortest_distance_point(handled_point)
        connected_points = find_connected_points(point_that_shortest_distance_no_in_handled_point)
        for connected_point in connected_points:
            ols_distance = dis[connected_point]
            new_distance = dis[point_that_shortest_distance_no_in_handled_point] + \
                           map[point_that_shortest_distance_no_in_handled_point][connected_point]
            if new_distance < ols_distance:
                dis[connected_point] = new_distance

        handled_point.append(point_that_shortest_distance_no_in_handled_point)

    print(dis)

#         find the shortest distance point,not included the handled point
