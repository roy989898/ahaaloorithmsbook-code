map = [
    [0, 2, 4, None, 10],
    [2, 0, 3, None, 7],
    [4, 3, 0, 4, 3],
    [None, None, 4, 0, 5],
    [10, 7, 3, 5, 0],
]

start = 0
aim = 4


def find_the_connect_point(cur_point: int):
    possible_points = map[cur_point]
    connected_points = []
    for point, point_mark in enumerate(possible_points):
        if point_mark != 0 and point_mark is not None:
            connected_points.append(point)

    return connected_points


def dfs(cur: int, points_pass: [int], dis: int):
    if cur == aim:
        print(dis, points_pass)
        return
    else:
        near_points = find_the_connect_point(cur)
        filtered_near_points = []
        for near_point in near_points:
            if near_point not in points_pass:
                filtered_near_points.append(near_point)

        if len(filtered_near_points) > 0:
            for filtered_near_point in filtered_near_points:
                a_b_distance = map[cur][filtered_near_point]
                dfs(filtered_near_point, points_pass + [filtered_near_point], dis + a_b_distance)

        else:
            return


if __name__ == '__main__':
    dfs(start, [start], 0)
