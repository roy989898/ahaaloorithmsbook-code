map = [
    [0, 1, 1, None, None],
    [1, 0, 1, 1, None],
    [1, 1, 0, 1, 1],
    [None, 1, 1, 0, 1],
    [None, None, 1, 1, 0],
]
start = 0
aim = 4
rounded_mark = [0] * len(map)


def find_the_connect_point(cur_point: int):
    possible_points = map[cur_point]
    connected_points = []
    for point, point_mark in enumerate(possible_points):
        if point_mark != 0 and point_mark is not None:
            connected_points.append(point)

    return connected_points


path = []
if __name__ == '__main__':
    q_points = [start]
    rounds = 0
    head = 0
    rounded_mark[start] = 0
    while not (head >= len(q_points)):
        cur_point = q_points[head]
        current_rounded_mark = rounded_mark[cur_point]
        if cur_point == aim:
            current_point = aim
            path.append(aim)
            print(rounded_mark[aim])
            print(rounded_mark)
            before_round = rounded_mark[aim] - 1
            while not (before_round < 0):
                match_rounded_mark_points = []
                for point, rounded_mark_item in enumerate(rounded_mark):
                    if rounded_mark_item == before_round:
                        match_rounded_mark_points.append(point)
                for match_rounded_mark_point in match_rounded_mark_points:
                    if map[match_rounded_mark_point][current_point] == 1:
                        current_point = match_rounded_mark_point
                        path.append(current_point)
                        break
                before_round = before_round - 1
            print("path: ", path)
        else:
            possible_points = find_the_connect_point(cur_point)
            filtered_possible_points = []
            for possible_point in possible_points:
                if possible_point not in q_points:
                    filtered_possible_points.append(possible_point)
                    rounded_mark[possible_point] = current_rounded_mark + 1
            q_points = q_points + filtered_possible_points
        head = head + 1
