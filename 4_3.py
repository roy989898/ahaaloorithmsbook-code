map = [
    [0, 0, 1, 0],
    [0, 0, 0, 0],
    [0, 0, 1, 0],
    [0, 1, 0, 0],
    [0, 0, 0, 1]
]
books = [
    [0, 0, 0, 0],
    [0, 0, 0, 0],
    [0, 0, 0, 0],
    [0, 0, 0, 0],
    [0, 0, 0, 0]
]


def check_point_available(x: int, y: int):
    return not (x < 0 or x > (len(map) - 1) or y < 0 or y > (
            len(map[0]) - 1)) and (map[x][y] != 1)


def get_next_points(current_point_x: int, current_point_y: int):
    possible_nexts = [[current_point_x, current_point_y + 1], [current_point_x + 1, current_point_y],
                      [current_point_x, current_point_y - 1], [current_point_x - 1, current_point_y]]
    next_points = []
    for possible_next in possible_nexts:
        if check_point_available(possible_next[0], possible_next[1]):
            next_points.append(possible_next)
    return next_points


def find_r():
    start_points = [[0, 0]]
    books[0][0] = 1
    find_the_aim = False
    aim = [3, 2]
    step = 0
    while not find_the_aim:
        # print(books)
        next_points = []
        for start_point in start_points:
            gen_next_points = get_next_points(start_point[0], start_point[1])
            for gen_next_point in gen_next_points:
                x = gen_next_point[0]
                y = gen_next_point[1]
                if (gen_next_point not in next_points) and (books[x][y] == 0):
                    next_points.append(gen_next_point)
                    # print("books_o: ", books)
                    books[x][y] = 1
                    # print("x:y ", x, ":", y)
                    # print("books: ", books)
                    # books[gen_next_point[0]][gen_next_point[1]] = 1
        step = step + 1
        print("step: ", step)
        print(next_points)
        # print(books)
        if aim in next_points or step > 10:
            find_the_aim = True
        else:
            start_points = next_points

    print(step)


if __name__ == '__main__':
    # print(books[1][1])
    find_r()
