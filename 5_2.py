map = [
    [0, 1, 1, None, 1],
    [1, 0, None, 1, None],
    [1, None, 0, None, 1],
    [None, 1, None, 0, None],
    [1, None, 1, None, 0],
]
booked_points = []


def find_the_connect_point(cur_point: int):
    possible_points = map[cur_point]
    connected_points = []
    for point, point_mark in enumerate(possible_points):
        if point_mark == 1:
            connected_points.append(point)

    return connected_points


def run(start: int):
    q_points = [start]
    head = 0
    while not (head >= len(q_points)):
        cur_point = q_points[head]
        print(cur_point)
        connected_points = find_the_connect_point(cur_point)
        filtered_connected_points = []
        for connected_point in connected_points:
            if connected_point not in q_points:
                filtered_connected_points.append(connected_point)
        q_points = q_points + filtered_connected_points
        head = head + 1


if __name__ == '__main__':
    run(0)
