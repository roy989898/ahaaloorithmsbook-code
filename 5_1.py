map = [
    [0, 1, 1, None, 1],
    [1, 0, None, 1, None],
    [1, None, 0, None, 1],
    [None, 1, None, 0, None],
    [1, None, 1, None, 0],
]
booked_points = []


def find_the_connect_point(cur_point: int):
    possible_points = map[cur_point]
    connected_points = []
    for point, point_mark in enumerate(possible_points):
        if point_mark == 1:
            connected_points.append(point)

    return connected_points


def dfs(cur: int):
    if cur not in booked_points:
        print(cur)
        booked_points.append(cur)
        points = find_the_connect_point(cur)
        if len(points) > 0:
            for filtered_point in points:
                dfs(filtered_point)
        else:
            return
    else:
        return


if __name__ == '__main__':
    dfs(0)
