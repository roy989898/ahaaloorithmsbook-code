import copy


def try_sort_min(nums: [int], biggets_num: int):
    init_array = [0 for i in range(biggets_num + 1)]
    result = []
    reversed_result = []

    # n
    for num in nums:
        init_array[num] = init_array[num] + 1
    # m
    for index, element in enumerate(init_array):
        # n
        for time in range(element):
            result.append(index)
    #         TODO reverse
    # m
    for i in range(len(result) - 1, -1, -1):
        reversed_result.append(result[i])

    return reversed_result


def bubble_sort_mine(nums: [int]):
    do_count = len(nums) - 1
    # loop n-1
    while do_count > 0:
        # loop n-1,n-2,n-3....until 0
        for check_index_1 in range(do_count):
            check_index_2 = check_index_1 + 1
            if nums[check_index_1] < nums[check_index_2]:
                check_index_1_num = nums[check_index_1]
                check_index_2_num = nums[check_index_2]
                nums[check_index_1] = check_index_2_num
                nums[check_index_2] = check_index_1_num
        do_count = do_count - 1
    return nums


def quick_sort(nums: [int]):
    if len(nums) > 1:
        ref_num = nums[0]
        middle = []
        right = []
        left = []
        for num in nums:
            if num > ref_num:
                right.append(num)
            elif num < ref_num:
                left.append(num)
            else:
                middle.append(num)

        return quick_sort(left) + middle + quick_sort(right)
    else:
        return nums


if __name__ == '__main__':
    nums = [12, 35, 99, 18, 76, 400, 879, 45, 1, 0, 8, 4, 6, 7]
    biggest_num = 3000
    # print(try_sort_min(nums, biggets_num=biggest_num))
    print(quick_sort(nums))
    # bubble_sort_mine(nums)
