map = [['#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#'],
       ['#', 'G', 'G', '.', 'G', 'G', 'G', '#', 'G', 'G', 'G', '.', '#'],
       ['#', '#', '#', '.', '#', 'G', '#', 'G', '#', 'G', '#', 'G', '#'],
       ['#', '.', '.', '.', '.', '.', '.', '.', '#', '.', '.', 'G', '#'],
       ['#', 'G', '#', '.', '#', '#', '#', '.', '#', 'G', '#', 'G', '#'],
       ['#', 'G', 'G', '.', 'G', 'G', 'G', '.', '#', '.', 'G', 'G', '#'],
       ['#', 'G', '#', '.', '#', 'G', '#', '.', '#', '.', '#', '#', '#'],
       ['#', '#', 'G', '.', '.', '.', 'G', '.', '.', '.', '.', '.', '#'],
       ['#', 'G', '#', '.', '#', 'G', '#', '#', '#', '.', '#', 'G', '#'],
       ['#', '.', '.', '.', 'G', '#', 'G', 'G', 'G', '.', 'G', 'G', '#'],
       ['#', 'G', '#', '.', '#', 'G', '#', 'G', '#', '.', '#', 'G', '#'],
       ['#', 'G', 'G', '.', 'G', 'G', 'G', '#', 'G', '.', 'G', 'G', '#'],
       ['#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#'], ]


# # is wall
# g is monster
# . is empty

def boom_kill_monster_number(x: int, y: int):
    min_x = 0
    min_y = 0
    max_x = len(map[0]) - 1
    max_y = len(map) - 1
    point = map[x][y]
    monster_num = 0
    if point != ".":
        return 0
    else:
        for u in range(x, min_x - 1, -1):
            if map[u][y] == '#':
                break
            elif map[u][y] == "G":
                monster_num = monster_num + 1

        for d in range(x, max_x + 1, 1):

            if map[d][y] == '#':
                break
            elif map[d][y] == "G":
                monster_num = monster_num + 1

        for l in range(y, min_y - 1, -1):

            if map[x][l] == '#':
                break
            elif map[x][l] == "G":
                monster_num = monster_num + 1

        for r in range(y, max_y + 1, 1):

            if map[x][r] == '#':
                break
            elif map[x][r] == "G":
                monster_num = monster_num + 1
        return monster_num


# handle right

if __name__ == '__main__':
    min_x = 0
    min_y = 0
    max_x = len(map[0]) - 1
    max_y = len(map) - 1

    most_kills = 0
    most_kills_x = 0
    most_kills_y = 0
    for x in range(min_x, max_x + 1, 1):
        for y in range(min_y, max_y + 1, 1):
            num = boom_kill_monster_number(x, y)
            if num > most_kills:
                most_kills = num
                most_kills_x = x
                most_kills_y = y
    print("most_kills: ", most_kills)
    print("most_kills_x: ", most_kills_x)
    print("most_kills_y: ", most_kills_y)
